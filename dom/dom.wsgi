#!/usr/bin/python
# -*- coding=utf-8 -*-

import bookdb
import urlparse



def application(environ, start_response):
    d = urlparse.parse_qs(environ.get('QUERY_STRING', 'Unset'))
    id = d.get('id', [''])


    response_body = """
    <html>
        <head>
            <meta charset="utf-8" />
            <title>Praca Domowa</title>
            </head>
            <body>
                <h2>Ksiazki</h2>"""


    for tytul in bookdb.BookDB().titles():

        if id[0]=='':
            #response_body += '<a href="http://localhost:4444/?id=' + tytul['id'] + '">' + tytul['title'] + '</a><br/>'
            response_body+='<li><a href="http://194.29.175.240:31018/?id='+tytul['id']+'">'+tytul['title']+'</a></li>'
            response_body+="""
                
            </body>
        </html>"""
        elif tytul['id'] == id[0]:
            response_body+="""
                 
            <body>
                <h2>Wybrana Ksiazka</h2>"""
            response_body += 'Tytul Ksiazki:' + bookdb.BookDB().title_info(id[0])['title'] + '</br>'
            response_body += 'ISBN Ksiazki:' + bookdb.BookDB().title_info(id[0])['isbn'] + '</br>'
            response_body += 'Publikacja Ksiazki: ' + bookdb.BookDB().title_info(id[0])['publisher'] + '</br>'
            response_body += 'Autor Ksiazki:' + bookdb.BookDB().title_info(id[0])['author'] + '</br>'
            response_body+="""
                   </body>
                </html>"""

    status = '200 OK'

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server

    srv = make_server('194.29.175.240', 31018, application)
    #srv = make_server('localhost', 4444, application)
    srv.serve_forever()