#!/usr/bin/python
# -*- coding: utf-8 -*-

import cgi
import cgitb
cgitb.enable()
import os
import datetime


print "Content-Type: text/html"
print

body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
        Nazwa serwera to %s.<br>
        <br>
        Adres serwera to %s:%s.<br>
        <br>
        Nazwa twojego komputera to %s.<br>
        <br>
        Przybywasz z %s:%s.<br>
        <br>
        Aktualnie wykonywany skrypt to <tt>%s</tt>.<br>
        <br>
        Żądanie przyszło o %s.<br>
    </body>
</html>""" % (
        os.environ['SERVER_NAME'], # Nazwa serwera
        os.environ['SERVER_ADDR'], # IP serwera
        os.environ['SERVER_PORT'], # port serwera
        'DAWID', # nazwa klienta

        os.environ['REMOTE_ADDR'], # IP klienta
        os.environ['REMOTE_PORT'], # port klienta
        os.environ['SCRIPT_NAME'], # nazwa skryptu
        datetime.date.today()
)

print body,