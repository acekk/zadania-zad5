#!/usr/bin/python
# -*- coding=utf-8 -*-

import datetime

body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
        Nazwa serwera to %s.<br>
        <br>
        Adres serwera to %s:%s.<br>
        <br>
        Nazwa twojego komputera to %s.<br>
        <br>
        Przybywasz z %s:%s.<br>
        <br>
        Aktualnie wykonywany skrypt to <tt>%s</tt>.<br>
        <br>
        Żądanie przyszło o %s.<br>
        <br>
        Ostatnia twoja wizyta miała miejsce %s.<br>
    </body>
</html>
"""

def application(environ, start_response):
    response_body = body % (
         environ.get('SERVER_NAME', 'Unset'), # nazwa serwera
         'blablabla', # IP serwera
         'zzz', # port serwera
         'cccc', # nazwa klienta
         'dddd', # IP klienta
         'eeee', # port klienta
         'ffff', # nazwa skryptu
         'gggg', # bieżący czas
         'hhhh', # czas ostatniej wizyty
    )
    status = '200 OK'

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('localhost', 4444, application)
    srv.serve_forever()